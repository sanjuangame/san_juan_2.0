﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public GameObject botoes;

	// Use this for initialization
	void Start () {
		Invoke ("showButtons", 3.1f);
	}
	void showButtons(){
		botoes.SetActive (true);
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void startGame(){
		SceneManager.LoadScene (1);
	}

	public void quitGame(){
	}

	public void aboutGame(){
	}

	public void configGame(){
	}



}
