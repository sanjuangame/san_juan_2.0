using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RetainItems : MonoBehaviour, ICollectable {

    private Dictionary<string, int> collectionItens;

    void Start()
    {
        collectionItens = new Dictionary<string, int>();
    }

    public IDictionary<string, int> items
    {
        get
        {
            return collectionItens;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Item"))
        {
            Collect(collider.gameObject);
        }

    }


    public void Collect(GameObject item)
    {
        if (collectionItens.ContainsKey(item.name))
        {
            collectionItens[item.name] += 1;
        }
        else
        {
            collectionItens.Add(item.name, 1);
        }
        Destroy(item);
    }
}
