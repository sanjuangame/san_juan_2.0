using UnityEngine;
using System.Collections;

public class WeaponRotate : MonoBehaviour {

    public int velocity = 2;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.parent.transform.Rotate(-Vector3.back * velocity);
	}
}
