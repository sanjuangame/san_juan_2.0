using UnityEngine;
using System.Collections;

public class Puzzles : MonoBehaviour {

    private Camera cam;
    public float zoomSpeed = 1f;
    public float finalOrthoSize = 3f;
    private bool enableZoomIn;
    private float initialOrthoSize;

    private AudioSource audioSource;

    private float[] value = { 0.4f, 0.6f, 0.8f, 1f };

    private const float MAX_INTERVAL_BETWEEN_PITCH = 4f;

    // Use this for initialization
    void Start () {
        cam = Camera.main;
        initialOrthoSize = cam.orthographicSize;
        enableZoomIn = false;
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        if (enableZoomIn)
        {
            ZoomIn();
            gameObject.GetComponent<Rhythm>().getRhytmn(audioSource);
        }
        else
        {
            ZoomOut();
            if (gameObject.GetComponent<Rhythm>())
            {
                 gameObject.GetComponent<Rhythm>().countBeatInput = 0;
            }
        }
    }

    // Verifica se o character esta na zona do puzzle
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            enableZoomIn = true;
            gameObject.AddComponent<Rhythm>().getPuzzle(audioSource, value);
        }
    }

    // Verifica se o character deixou a zona do puzzle
    void OnTriggerExit2D(Collider2D collider)
    {
        enableZoomIn = false;
    }

    // Realiza um ZoomIn no local onde o puzzle esta localizado
    void ZoomIn()
    {
        if (cam.orthographicSize > finalOrthoSize)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, finalOrthoSize, Time.deltaTime * zoomSpeed);
        }
    }

    // Realiza um ZoomOut para a posição inicial da camera
    void ZoomOut()
    {
        if (cam.orthographicSize < initialOrthoSize)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, initialOrthoSize, Time.deltaTime * zoomSpeed);
        }
    }

}
