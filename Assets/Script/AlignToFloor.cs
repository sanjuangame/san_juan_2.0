using UnityEngine;
using System.Collections;

public class AlignToFloor : MonoBehaviour {

    private Vector3 normal;
    private Quaternion iniRotate;


	// Use this for initialization
	void Start () {
        iniRotate = transform.rotation;
        normal = Vector3.up;
	}
	
	// Update is called once per frame
	void Update () {

        RaycastHit hit;

        Debug.DrawRay(transform.position, Vector3.up, Color.blue );

        if (Physics.Raycast(transform.position,-normal, out hit))
        {
            normal = Vector3.Lerp(normal, hit.normal, 4 * Time.deltaTime);
            Quaternion rot = Quaternion.FromToRotation(Vector3.up, normal);
            Debug.Log("rotation: "+rot);
            transform.rotation = rot * iniRotate;
        }
	}
}
