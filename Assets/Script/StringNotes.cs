using UnityEngine;
using System.Collections;

public class StringNotes : MonoBehaviour {

    private const float LAUCH_SENSE_VALUE = 1;
    private const float INTERVAL = 3;

    private float timeLeft;
    private bool touchString;
    private float horizontalForce = 0f;
    private GameObject character;
    private bool lauched;
    private float characterSense = 1f;

    private AudioClip soundOne;
    private AudioClip soundTwo;
    private AudioSource audio;
    private bool controlColor = true;

    public float verticalForce = 2000f;
    public float velocity = 0.2f;
    public LauchSense lauchSense = LauchSense.LEFT_TO_RIGHT;


    public enum LauchSense
    {
        LEFT_TO_RIGHT,RIGHT_TO_LEFT,BOTH
    }


    void Start () {
        timeLeft = INTERVAL;
        soundOne = Resources.Load("Sounds/E guitar") as AudioClip;
        soundTwo = Resources.Load("Sounds/G guitar") as AudioClip;
    }

	
	void Update () {

        if (RegressiveTimeCount())
        {
            PlayString();
        }
    }

    // retorna false quando a contagem regressiva chega ao fim
    bool RegressiveTimeCount()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            return true;
        }
        return false;
    }


    void FixedUpdate()
    {
        if (touchString)
        {         
            if (!character.GetComponent<CharacterController>().grounded)
            {
                character.transform.Translate(new Vector3(getLauchSense(), 0, 0) * velocity);
                lauched = true;
            }
            else if(lauched)
            {
                touchString = false;
                character = null;
            }
            
        }   
    }

    // retorna a dire��o do lan�amento da corda 
    private float getLauchSense()
    {
        float sense = 0;
        switch (lauchSense)
        {
            case LauchSense.LEFT_TO_RIGHT:
                {
                    return sense = LAUCH_SENSE_VALUE;
                }
            case LauchSense.RIGHT_TO_LEFT:
                {
                    return sense = -LAUCH_SENSE_VALUE;
                }
            default:
                {
                    return sense = characterSense;
                }
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        //AudioSource audio = GetComponent<AudioSource>();
        //audio.Play();

        if (character==null)
        {
            character = other.gameObject;
            character.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontalForce, verticalForce));
            touchString = true;
            lauched = !touchString;
            characterSense = character.GetComponent<CharacterController>().getDisplacementSense(character.GetComponent<CharacterController>().horizontalDisplacement);
        }
        
    }

    // toca as notas da corda
    void PlayString()
    {
        audio = GetComponent<AudioSource>();      
        controlColor = !controlColor;
        ChangeStringState();
        timeLeft = INTERVAL;
        audio.Play();
    }

    // Muda o estado da corda
    void ChangeStringState()
    {
        Color color;
        GameObject vibrationString = GameObject.Find("string");

        if (controlColor)
        {
            audio.clip = soundOne;
            color = Color.green;
        }
        else
        {
            audio.clip = soundTwo;
            color = Color.blue;
        }
        vibrationString.GetComponent<SpriteRenderer>().color = color;
    }
}
