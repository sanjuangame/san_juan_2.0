using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RepeatMelody : Puzzle, IPuzzleable
{

    private ArrayList notesList = new ArrayList { "C", "D", "E", "F", "G", "A", "B" };
    private float soundInterval = 0.8f;
    private ArrayList randomAudioNotes;

    private bool firstBeat = true;
    private int count;
    private AudioSource audioSource;
    private AudioClip clip;
    private string directoryAudioPath = "Sounds/Musical_Notes/";

    private int SECONDS_BEFORE_BEGIN_PUZZLE = 2;
    private int NUMBER_SOUNDS = 4;

    private IDictionary<KeyCode, string> inputKeyNote;
    private ArrayList KeyInputSound;
    private int countInput;


    void Awake()
    {
        count = 0;
        countInput = 0;
        randomAudioNotes = new ArrayList();
        audioSource = gameObject.GetComponent<AudioSource>();
        inputKeyNote = new Dictionary<KeyCode, string>();
        KeyInputSound = new ArrayList();
        InicializedNotes();
    }

    void InicializedNotes()
    {
        inputKeyNote.Add(KeyCode.C, "C");
        inputKeyNote.Add(KeyCode.D, "D");
        inputKeyNote.Add(KeyCode.E, "E");
        inputKeyNote.Add(KeyCode.F, "F");
        inputKeyNote.Add(KeyCode.G, "G");
        inputKeyNote.Add(KeyCode.A, "A");
        inputKeyNote.Add(KeyCode.B, "B");
    }
    // Update da classe filha
    public override void ChildUpdate()
    {
        if (base.EnableZoomIn)
        {
            KeyInput();
        }
    }

    public override void ChildOnTriggerEnter()
    {
        Challenger();
    }

    public override void ChildOnTriggerExit()
    {
        KeyInputSound = new ArrayList();
        randomAudioNotes = new ArrayList();
    }

    public void Challenger()
    {
        StartCoroutine(MelodyPuzzle());
    }


    private IEnumerator MelodyPuzzle()
    {
        if (count < NUMBER_SOUNDS)
        {
            yield return new WaitForSeconds(soundInterval);
            if (!firstBeat)
                LoadAudioClip((string) notesList[RandomPitch()]);
            
            count++;
            firstBeat = false;
            StartCoroutine(MelodyPuzzle());
        }
        else
        {
            count = 0;
            firstBeat = true;
        }
    }

    void LoadAudioClip(string audio)
    {
        clip = Resources.Load(directoryAudioPath + audio) as AudioClip;
        if (clip != null)
        {
            Debug.Log("Carregou a nota " + audio);
            audioSource.clip = clip;
            audioSource.Play();
            randomAudioNotes.Add(audio);
        }
    }

    // Gera um sorteio randomico no array com intervalos (em segundos)
    private int RandomPitch()
    {    
        if (firstBeat)
            return SECONDS_BEFORE_BEGIN_PUZZLE;
        //So esta retornando 0, verificar
        int random = UnityEngine.Random.Range((int)0, notesList.Count - 1);
        Debug.Log("Random Number: " + random);

        return random;
    }

    public void Solution()
    {
        CompareSound();
    }

    // Calcula a frequenia em que a tecla foi pressionada
    void KeyInput()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            LoadAudioClip(inputKeyNote[KeyCode.C]);
            KeyInputSound.Add(inputKeyNote[KeyCode.C]);
            countInput++;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            LoadAudioClip(inputKeyNote[KeyCode.D]);
            KeyInputSound.Add(inputKeyNote[KeyCode.D]);
            countInput++;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            LoadAudioClip(inputKeyNote[KeyCode.E]);
            KeyInputSound.Add(inputKeyNote[KeyCode.E]);
            countInput++;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            LoadAudioClip(inputKeyNote[KeyCode.F]);
            KeyInputSound.Add(inputKeyNote[KeyCode.F]);
            countInput++;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            LoadAudioClip(inputKeyNote[KeyCode.G]);
            KeyInputSound.Add(inputKeyNote[KeyCode.G]);
            countInput++;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            LoadAudioClip(inputKeyNote[KeyCode.A]);
            KeyInputSound.Add(inputKeyNote[KeyCode.A]);
            countInput++;
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            LoadAudioClip(inputKeyNote[KeyCode.B]);
            KeyInputSound.Add(inputKeyNote[KeyCode.B]);
            countInput++;
        }

        if (KeyInputSound.Count == NUMBER_SOUNDS-1)
        {
            Solution();
        }
    }

    void CompareSound()
    {
        for (int i = 0; i < NUMBER_SOUNDS-1; i++)
        {
            Debug.Log("Comparacao: " + randomAudioNotes[i] + " = " + KeyInputSound[i] + " indice: " + i);
            Debug.Log("Sao iguais: " + randomAudioNotes[i].Equals(KeyInputSound[i]));
        }
        KeyInputSound = new ArrayList();
        randomAudioNotes = new ArrayList();
    }
}
