using UnityEngine;
using System.Collections;


public class RepeatRhythm : Puzzle, IPuzzleable {

    // Atributos output
    private int count;
    private bool firstBeat = true;
    private float[] soundInterval = { 0.4f, 0.6f, 0.8f, 1f };
    private ArrayList timeBetweenGeneratedPitch;
    private AudioSource audioSource;

    private int NUMBER_PITCH = 3;
    private const float SECONDS_BEFORE_BEGIN_PUZZLE = 2f;
    private const float COMPARISON_THRESHOLD = 0.1f;

    // Atributos do Input
    public int countBeatInput;
    private float startTime;
    private float endTime;
    private float intervalAmount;
    private ArrayList keyTimeInterval;
    private bool enableCountTime;
    private float interval;

    // Use this for initialization
    void Awake()
    {
        timeBetweenGeneratedPitch = new ArrayList();
        keyTimeInterval = new ArrayList();
        count = countBeatInput = 0;
        intervalAmount = NUMBER_PITCH - 1;
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    // Update da classe filha
    public override void ChildUpdate()
    {
        if (base.EnableZoomIn)
        {
            getRhytmn();
        }
        else
        {
            countBeatInput = 0;
        }
    }

    // Metodo sobreesrito com a funcao de iniciar o desafio quando na área do puzzle
    public override void ChildOnTriggerEnter()
    {
        Challenger();
    }

    // Criar o desafio do puzzle
    public void Challenger()
    {
        if(audioSource)
            StartCoroutine(RhythmPuzzle());
    }

    // Gera ritmos randomicos baseados em intervalos de tempo pre-definidos (soundInterval)
    private IEnumerator RhythmPuzzle()
    {
        if (count < NUMBER_PITCH)
        {
            yield return new WaitForSeconds(RandomPitch());
            audioSource.Play();
            Debug.Log("AQUIIII: " + count);
            count++;
            firstBeat = false;
            StartCoroutine(RhythmPuzzle());
        }
        else
        {
            count = 0;
            firstBeat = true;
        }
    }

    // Gera um sorteio randomico no array com intervalos (em segundos)
    private float RandomPitch()
    {
        if (firstBeat)
            return SECONDS_BEFORE_BEGIN_PUZZLE;

        int random = Random.Range(0, soundInterval.Length);
        timeBetweenGeneratedPitch.Add(soundInterval[random]);

        return soundInterval[random];
    }

    // Prover a solução do Puzzle
    public void Solution()
    {
        CompareTimes();
    }

    // Verifica o ritmo do input e se eh possivel comparar
    public void getRhytmn()
    {
        if (countBeatInput < NUMBER_PITCH)
        {
            KeyFrequencyInput();

            if (keyTimeInterval.Count == intervalAmount)
            {
                Solution();
            }
        }
    }

    // Calcula a frequenia em que a tecla foi pressionada
    void KeyFrequencyInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Tecla R");
            audioSource.Play();
            endTime = Time.time;
            CountTime();
            countBeatInput++;
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            enableCountTime = true;
            startTime = Time.time;
        }
    }

    // Conta o intervalo entre as batidas do input para comparar o ritmo
    void CountTime()
    {
        if (enableCountTime)
        {
            interval = endTime - startTime;

            if (keyTimeInterval.Count < intervalAmount)
            {
                keyTimeInterval.Add(interval);
            }
            startTime = 0;
            enableCountTime = false;
        }
    }

    // Compara as batidas geradas automatiamente com o input do usuario
    void CompareTimes()
    {
        Debug.Log("Size: " + timeBetweenGeneratedPitch.Count + " Size: " + keyTimeInterval.Count);
        for (int i = 0; i < intervalAmount; i++)
        {
            Debug.Log("Comparacao: " + timeBetweenGeneratedPitch[i] + " = " + keyTimeInterval[i] + " indice: " + i);
            Debug.Log("Sao iguais: " + GameUtilities.CompareFloat((float)timeBetweenGeneratedPitch[i], (float)keyTimeInterval[i], COMPARISON_THRESHOLD));
        }

        keyTimeInterval = new ArrayList();
    }


}
