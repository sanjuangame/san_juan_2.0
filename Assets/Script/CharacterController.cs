using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharacterController : MonoBehaviour, IKillable {

    [HideInInspector]
    public bool jump;
	bool chao;
    public bool enableMultiJump;
    public int MULTI_JUMP = 2;
    private int countMultiJump = 0; 
	bool rightSide = false;
	Animator animator;
	public Text textLives;
	public float moveForce = 300f;
    public float maxSpeed = 5f;
    public float jumpForce = 0.2f;
    public Transform characterCollider;
    [HideInInspector]
    public bool grounded;
    private Rigidbody2D rigibody;
    private Transform groundCheck;
    const float groundedRadius = .2f;
    public LayerMask groundLayer;
    public int jumpForceLimit = 8;
    private int jumpCount;
	public float velocidade = 1000f;
	float currentDashTime;
	private bool dash = false;
    [HideInInspector]
    public float horizontalDisplacement = 1f;
	private float horizontalSense = 1f;
	private int itemCount;
	private bool isPunching = false;

	public	float maxDashTime= 1.0f;
	public float dashSpeed= 0f;
	public float dashStoppingSpeed= 0.1f;
	public Vector3 moveDirection;

    void Awake()
    {
		currentDashTime = maxDashTime;

		rigibody = GetComponent<Rigidbody2D>();
        groundCheck = transform.Find("groundCheck");
    }

    // Use this for initialization
    void Start()
    {
		textLives.text = "LIVES: "+Global.Lives;
		animator = GetComponent<Animator> ();

    }

	void flip(){
		rightSide = !rightSide;
		Vector2 auxScale = new Vector2 (transform.localScale.x * -1, transform.localScale.y);
		transform.localScale = auxScale;
	}

    // Update is called once per frame
    void Update()
    {
		
		grounded = false;



        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, groundLayer);
		chao = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, groundLayer);

		animator.SetBool ("onGround", chao);

		foreach (Collider2D collider in colliders)
        {
			if (1 << collider.gameObject.layer == groundLayer.value) {
				grounded = true;
				animator.SetBool ("onGround", true);
			}

        }

        if (grounded)
            countMultiJump = 0;

        if (Input.GetButtonDown("Jump") && (grounded || enableMultiJump))
         {
            animator.SetBool ("onGround", false);
			animator.SetFloat ("velocityY", rigibody.velocity.y);
			if(countMultiJump < MULTI_JUMP)
                jump = true;
			
         }

        // atinge o inimigo
        PushEnemy();

        // Realiza o dash do character
        MakeDash();

        // Alinha o "character" (quadrado) com o ch�o
//        AlignToFloor();

        // Exibe a lista de itens coletados no console
        if (Input.GetKeyDown(KeyCode.I))
        {
            foreach(KeyValuePair<string,int> item in GetComponent<RetainItems>().items)
            {
                Debug.Log("Nome: "+ item.Key+ " Quantidade: "+ item.Value );
            }
            
        }

        // Atualiza o numero de itens coletados
        UpdateHud();

    }

    void UpdateHud()
    {
        foreach (KeyValuePair<string, int> item in GetComponent<RetainItems>().items)
        {
            if (item.Key.Equals("Item"))
            {
                itemCount = item.Value;
            }
        }
    }

    void PushEnemy()
    {
        //Begin pushing code
        if (Input.GetKeyDown(KeyCode.Q) && !isPunching)
        {
            isPunching = true;
            punch();
        } // End pushing code

    }

    void MakeDash()
    {
        //Begin dash code
        if (Input.GetKeyDown(KeyCode.Z))
        {
            currentDashTime = 0f;

            Debug.Log("CurrentDASh: " + currentDashTime);
            Debug.Log("MAXDASh: " + maxDashTime);
            Debug.Log("dashStopping: " + dashStoppingSpeed);
            Debug.Log("DIRECAO MOVI: " + moveDirection);
            Debug.Log("XXXX: " + horizontalSense);
            currentDashTime = 0f;
            dashSpeed = 20.0f;

            animator.SetBool("dash", true); 
            Invoke ("stopDash", 1f);
        }
        if (currentDashTime < maxDashTime)
        {
            moveDirection = new Vector3(0, 0, dashSpeed);
            currentDashTime += dashStoppingSpeed;
              
        }
        else
        {
            moveDirection = Vector3.zero;
            dashSpeed = 0f;

        }   
        if(!rightSide)
           characterCollider.transform.Translate(Vector2.right * dashSpeed * Time.deltaTime);
        else
           characterCollider.transform.Translate(Vector2.left * dashSpeed * Time.deltaTime);
        
        // End dash code
    }

	void stopDash(){
		// somente para a animacao
		animator.SetBool("dash",false);

	}


    void AlignToFloor()
    {
        RaycastHit2D hit;
        float angle;

        hit = Physics2D.Raycast(groundCheck.transform.position, Vector2.down);

        if (hit && !dash)
        {
            angle = Vector2.Angle(hit.normal, transform.up);
            Vector3 result = Vector3.Cross(hit.normal, transform.up).normalized;

            if (grounded)
            {
                float roundNumber = angle;
                Quaternion quarte = Quaternion.Euler(0, 0, -(roundNumber + 26f) * result.z);
                transform.rotation = Quaternion.Slerp(transform.rotation, quarte, .2f);
            }
            else
            {
                Quaternion quarte = Quaternion.Euler(0, 0, 0);
                transform.rotation = quarte;
            }
        }
    }

    
	void punch(){
		Quaternion quarte = Quaternion.Euler(0, 0, 90);
		transform.rotation = quarte;

		Invoke ("releasePunch",1f);
	}

	void releasePunch(){
		isPunching = false;
	}


    void FixedUpdate()
    {	
		animator.SetBool("onGround", grounded);


		horizontalDisplacement = Input.GetAxis ("Horizontal");

		if (horizontalDisplacement > 0 && rightSide) {
			flip ();
		} else if(horizontalDisplacement < 0 && !rightSide) {
			flip ();

		}

        horizontalSense = getDisplacementSense(horizontalDisplacement);
		animator.SetFloat("velocity", Mathf.Abs(horizontalDisplacement));

        rigibody.velocity = new Vector2(horizontalDisplacement * maxSpeed, rigibody.velocity.y);
      
        if (jump)
        {
			ManageJump();
        }
    }

    // retorna a sentido do deslocamento do personagem
    public float getDisplacementSense(float direction)
    {
        float sense = 1f;
        if (!dash)
        {
            if (horizontalDisplacement > 0)
            {
                sense = 1f;
            }
            else if (horizontalDisplacement < 0)
            {
                sense = -1f;
            }
        }
        return sense;
    }

	void OnGUI()
	{
		Texture2D t = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/item.png", typeof(Texture2D));
		float xPosition = 1;
		GUI.DrawTexture(new Rect(xPosition,1,20,20),t);
		GUI.Label(new Rect(xPosition + 25f,1,200,30), " X " + itemCount);      

    }

    /******************************* PULOS DO CHARACTER ************************/

    // Gerencia os pulos do game character
    void ManageJump()

    {

        if (enableMultiJump)
            MultiJump();
        else
            ProgressiveJump();
    }

    // Realiza um pulo de intensidade progressiva (De acordo com o tempo em que a tecla se mantem pressionada)
    void ProgressiveJump()
    {
        if (Input.GetButton("Jump") && jumpCount < jumpForceLimit)
        {
            jumpCount++;
            //Debug.Log("Jump" + jumpCount);
            rigibody.AddForce(new Vector2(0f, jumpForce));



        }
        else
        {
            jump = false;

            jumpCount = 0;
        }
    }

    // Realiza um pulo de intensidade constante
    void ConstantJump()
    {
        rigibody.AddForce(new Vector2(0f, 2000f));
        jump = false;


    }

    // Gerencia os multiplos pulos a serem realizados
    void MultiJump()
    {
        if (countMultiJump == 0) 
            ProgressiveJump();
        else
            ConstantJump();

        if(!jump)
            countMultiJump++;
    }



    public void Kill(GameObject enemy){
		if (isPunching) {
			Destroy (enemy);
		} else {
			
			if (Global.Lives > 0) {
				Destroy (gameObject);
				Scene scene = SceneManager.GetActiveScene (); 
				SceneManager.LoadScene (scene.name);
				Global.Lives--;
				textLives.text = "LIVES: "+Global.Lives;
			} else {
				textLives.text = "GAMEOVER";

				SceneManager.LoadScene ("Menu");
			}
		}
    }

}
