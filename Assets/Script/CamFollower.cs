using UnityEngine;
using System.Collections;

public class CamFollower : MonoBehaviour {

    public Transform target;
    public float distance = 10f;
    public GameObject background;

    private Vector3 camPosition;
    private Renderer rend;
    private float leftLimitX, rightLimitX, halfCamFOV;
    private float targetLimitLeft;
    private float targetLimitRight;

    // Use this for initialization
    void Start ()
    {
        rend = background.GetComponent<Renderer>();
        halfCamFOV = Camera.main.orthographicSize * Screen.width / Screen.height;
        // calcula a distancia do background para a borda da camera
        leftLimitX = rend.bounds.min.x + halfCamFOV;
        rightLimitX = rend.bounds.max.x - halfCamFOV;
        // calcula o limite em que o player pode se movimentar no cenario
        targetLimitLeft = rend.bounds.min.x + target.GetComponent<BoxCollider2D>().size.x;
        targetLimitRight = rend.bounds.max.x - target.GetComponent<BoxCollider2D>().size.x;
    }

    
    void Update()
    {
        if (target.transform.position.x <= targetLimitLeft)
        {
            target.position = new Vector3(targetLimitLeft, target.position.y, target.position.z);
        }else if (target.transform.position.x >= targetLimitRight)
        {
            target.position = new Vector3(targetLimitRight, target.position.y, target.position.z);
        }
    }
    

    void LateUpdate()
    {
        if (target != null)
        {
            transform.position = Following();
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftLimitX, rightLimitX), transform.position.y, transform.position.z);
        }
    }

    Vector3 Following()
    {
        return  new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z - distance);
    }
}
