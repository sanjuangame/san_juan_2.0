using UnityEngine;
using System.Collections;
using System.Timers;

public static class GameUtilities  {

    private static float timeLeft;
    private static bool beginCount = true;

    public static void SequenceRhythm(AudioSource source)
    {
        source.PlayDelayed(5);
        Debug.Log(5);

    }

    public static bool TimeCount(float time)
    {
        if (beginCount)
        {
            timeLeft = time;
        }
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            return true;
        }
        beginCount = false;
        return beginCount;
    }

    public static bool CompareFloat(float num1, float num2, float threshold)
    {
        return ((num1 < num2) ? (num2 - num1) : (num1 - num2)) <= threshold;
    }

}
