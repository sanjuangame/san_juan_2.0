﻿using System;
using UnityEngine;

public class SoundPlayer
{   
    private AudioSource audioSource;  
    private AudioClip audioClip;   

    public SoundPlayer(AudioSource audioSource, AudioClip audioClip)
    {
      this.audioSource = audioSource;   
      this.audioClip = audioClip;
    }

    public AudioClip AudioClip
    {
        get
        {
            return audioClip;
        }
        set
        {
            audioClip = value;
        }   
    }

    public void PlaySound()
    {
        audioSource.clip = audioClip;
        audioSource.volume = 1f;
        audioSource.Play();
    }
}


