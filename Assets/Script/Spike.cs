﻿using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.CompareTag("Player"))
		{
			CharacterController characterController = (CharacterController) coll.gameObject.GetComponent(typeof(CharacterController));
			characterController.Kill(gameObject);
//			deadString = transform.GetComponentInParent<ParentReference>().reference;
	//		deadString.SetActive(true);
		}

	}
}
