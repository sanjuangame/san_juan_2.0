﻿using UnityEngine;
using System.Collections;

public class NoteBlock : MonoBehaviour {

	private bool onPlataform = false;

	public bool OnPlataform {
		get {
			return this.onPlataform;
		}
		set {
			onPlataform = value;
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		onPlataform = true;
	}

	void OnTriggerExit2D(Collider2D col)
	{
		onPlataform = false;
	}
}
