﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PuzzleNoteController : MonoBehaviour {

	private Dictionary<string, LinkedList<Dictionary<string,string>>> notesMap;
	public AudioClip audioClip;
	private SoundPlayer soundPlayer;
	public Dictionary<string, LinkedList<Dictionary<string, string>>> NotesMap {
		get {
			return this.notesMap;
		}
		set {
			notesMap = value;
		}
	}
	// Use this for initialization
	void Start () {
		notesMap = new Dictionary<string, LinkedList<Dictionary<string,string>>>();
        string baseFolder = "Sounds/notes/";
		Dictionary<string,string> noteA = new Dictionary<string,string>();
        noteA.Add("A", baseFolder + "A");

		Dictionary<string,string> noteB = new Dictionary<string,string>();
        noteB.Add("B", baseFolder + "B");

		Dictionary<string,string> noteC = new Dictionary<string,string>();
        noteC.Add("C", baseFolder + "C");

		Dictionary<string,string> noteD = new Dictionary<string,string>();
        noteD.Add("D", baseFolder + "D");

		Dictionary<string,string> noteE = new Dictionary<string,string>();
        noteE.Add("E", baseFolder + "E");

		Dictionary<string,string> noteF = new Dictionary<string,string>();
		noteF.Add("F", baseFolder + "F");

		Dictionary<string,string> noteFSustenido = new Dictionary<string,string>();
        noteFSustenido.Add("F#", baseFolder + "F#");

		Dictionary<string,string>[] noteVariableFSustenidoArray = {noteD, noteFSustenido, noteC};
		LinkedList<Dictionary<string,string>> noteVariableFSustenidoList = new LinkedList<Dictionary<string,string>>(noteVariableFSustenidoArray);

		Dictionary<string,string>[] noteVariableEArray = {noteD, noteE, noteA};
		LinkedList<Dictionary<string,string>> noteVariableEList = new LinkedList<Dictionary<string,string>>(noteVariableEArray);

		notesMap.Add("F#Variable1",noteVariableFSustenidoList);
		notesMap.Add("F#Variable2",noteVariableFSustenidoList);
		notesMap.Add("EVariable",noteVariableEList);
	}
}
