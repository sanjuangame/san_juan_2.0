﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicalNote : MonoBehaviour
{
    private Dictionary<string, LinkedList<Dictionary<string,string>>> notesMap;
    public AudioClip audioClip;
    private SoundPlayer soundPlayer;

    void Start()
    {
        soundPlayer = new SoundPlayer(gameObject.AddComponent<AudioSource>(),audioClip);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("VariableNote"))
            {
				CharacterController charController = col.gameObject.GetComponent( typeof(CharacterController) ) as CharacterController;
				NoteBlock noteBlock = GameObject.Find("BlockPuzzle").GetComponent( typeof(NoteBlock) ) as NoteBlock;
				PuzzleNoteController puzzleNoteController = GameObject.Find ("Puzzle").GetComponent (typeof(PuzzleNoteController)) as PuzzleNoteController;
				Debug.Log ("NO CHAO? " + noteBlock.OnPlataform);
				if (!noteBlock.OnPlataform){
					notesMap = puzzleNoteController.NotesMap;
                    LinkedList<Dictionary<string,string>> noteList = notesMap[gameObject.name];
                    LinkedListNode<Dictionary<string,string>> firstNote = noteList.First;
                    List<string> lList = new List<string>(firstNote.Value.Keys);
                    string key = lList[0];

                    gameObject.GetComponentInParent<TextMesh>().text = key;
                    audioClip = Resources.Load(noteList.First.Value[key]) as AudioClip;

                    noteList.RemoveFirst();
                    noteList.AddLast(firstNote);

                    Debug.Log(gameObject.name);
                    soundPlayer.AudioClip = audioClip;
                }
            }
            soundPlayer.PlaySound();
        }

    }
}

