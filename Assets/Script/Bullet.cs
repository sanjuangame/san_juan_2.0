﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    public float bulletSpeed = 5f;
    private float originalXPosition;

    // Use this for initialization
    void Start()
    {
        originalXPosition = transform.position.x;
    }
	
    // Update is called once per frame
    void Update()
    {
	    MoveBullet();
    }

    void MoveBullet()
    {
         
        transform.Translate(bulletSpeed * Vector3.left * Time.deltaTime);
        if (transform.position.x <= originalXPosition - 10f)
        {
            Destroy(gameObject);
        }
    }
}

