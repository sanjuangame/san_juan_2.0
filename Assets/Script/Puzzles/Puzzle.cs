using UnityEngine;
using System.Collections;

public class Puzzle : MonoBehaviour
{

    private Camera cam;
    public float zoomSpeed = 1f;
    public float finalOrthoSize = 3f;
    private bool enableZoomIn;
    private float initialOrthoSize;

    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        initialOrthoSize = cam.orthographicSize;
        enableZoomIn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (enableZoomIn)
        {
            ZoomIn();
        }
        else
        {
            ZoomOut();
        }

        ChildUpdate();
    }

    virtual public void ChildUpdate()
    {

    }

    // Verifica se o character esta na zona do puzzle
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            enableZoomIn = true;
            ChildOnTriggerEnter();
        }
    }

    public virtual void ChildOnTriggerEnter()
    {

    }

    // Verifica se o character deixou a zona do puzzle
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.name.Equals("Player"))
        {
            enableZoomIn = false;
            ChildOnTriggerExit();
        }
    }

    public virtual void ChildOnTriggerExit()
    {

    }

    // Realiza um ZoomIn no local onde o puzzle esta localizado
    void ZoomIn()
    {
        if (cam.orthographicSize > finalOrthoSize)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, finalOrthoSize, Time.deltaTime * zoomSpeed);
        }
    }

    // Realiza um ZoomOut para a posição inicial da camera
    void ZoomOut()
    {
        if (cam.orthographicSize < initialOrthoSize)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, initialOrthoSize, Time.deltaTime * zoomSpeed);
        }
    }

    public bool EnableZoomIn
    {
        get { return enableZoomIn; }
    }
}
