﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class RetractileSpike : Spike
	{
		public float hideVelocity = 1f;
		public float showVelocity = 5f;
		public float positionDiff = 3.8f;

		private bool movingLeft = false;
		private float initialPosition;
		private float maxPosition;

		public RetractileSpike ()
		{
			
		}

		void Start(){
			initialPosition = transform.position.x;
			maxPosition = initialPosition + positionDiff;
			Debug.Log("MAX POSITION: " + maxPosition);
		}

		void Update(){
			
		}

		void FixedUpdate(){
			Retract();
		}

		private void Retract(){
			if (movingLeft){
				transform.Translate(Vector2.up * showVelocity * Time.deltaTime);
				if(transform.position.x <= initialPosition)
				{
					movingLeft = false;
				}
			}
			else{
				transform.Translate(Vector2.down * hideVelocity * Time.deltaTime);
				if(transform.position.x >= maxPosition)
				{
					movingLeft = true;
				}
			}
		}
	}
}

