﻿using UnityEngine;
using System.Collections;

public class RecordPlayer : MonoBehaviour {

    public AudioClip audioClip;
    private bool isPlayingRecord;
    private AudioSource audioSource;
    private SoundPlayer soundPlayer;

	// Use this for initialization
	void Start () {
         soundPlayer = new SoundPlayer(gameObject.AddComponent<AudioSource>(), audioClip);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.X) && !isPlayingRecord)
        {
            isPlayingRecord = true;
        }
	}

    void OnTriggerStay2D (Collider2D col)
    {
        if(col.gameObject.name == "Player" && isPlayingRecord)
        {
            soundPlayer.PlaySound();
            isPlayingRecord = false;
        }
    }

}
