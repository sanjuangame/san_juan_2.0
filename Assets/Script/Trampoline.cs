using UnityEngine;
using System.Collections;

public class Trampoline : MonoBehaviour {

    private AudioClip soundOne;
    public float verticalForce = 10f;
    private float horizontalForce = 0f;
    //private AudioSource audio;
    private bool touchString;

    private GameObject character;

    // Use this for initialization
    void Start()
    {
        soundOne = Resources.Load("Sounds/E guitar") as AudioClip;
    }

    void FixedUpdate()
    {
        if (touchString)
        {
            StartCoroutine(PushCharacter());
        }
    }

    /*Utilizar esse caso queira que o personagem passe por dentro do trampolim*/
    void OnTriggerEnter2D(Collider2D other)
    {
        //AudioSource audio = GetComponent<AudioSource>();
        //audio.Play();

        character = other.gameObject;
        character.GetComponent<Rigidbody2D>().AddForce(new Vector2(horizontalForce, verticalForce));
        touchString = true;

    }

    /*Utilizar esse caso queira que o personagem possa se chocar com o trampolim*/
    void OnCollisionEnter2D(Collision2D other)
    {
        //AudioSource audio = GetComponent<AudioSource>();
        //audio.Play();

        character = other.gameObject;
        character.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 10f));
        touchString = true;
    }

    
    IEnumerator PushCharacter()
    {
        for (int i = 0;i < 10; i++)
        {
            character.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f , 20f));
            yield return null;
        }
        
        touchString = false;
    }

}
