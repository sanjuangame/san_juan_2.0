﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShooterEnemy : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
       spawnBullets();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void spawnBullets()
    {
        InvokeRepeating("addBullet",0.0f,1.0f); 
    }

    void addBullet()
    {
        GameObject bullet = Instantiate(Resources.Load("GameObjects/EnemyBullet")) as GameObject;
        Vector3 bulletPosition = new Vector3(transform.position.x - 1f, transform.position.y);
        bullet.transform.position = bulletPosition;
    }
}
