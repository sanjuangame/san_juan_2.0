using UnityEngine;
using System.Collections;

public class ManagePuzzle : MonoBehaviour
{
    //private IPuzzleable puzzle;

    public PuzzleName puzzleType = PuzzleName.REPEAT_RHYTHM;

    public enum PuzzleName
    {
        REPEAT_RHYTHM, REPEAT_MELODY
    }
     
    // Use this for initialization
    void Start()
    {
        AttachScript();
    }

    void AttachScript()
    {
        switch (puzzleType)
        {
            case PuzzleName.REPEAT_RHYTHM:
                {
                    gameObject.AddComponent<RepeatRhythm>();
                    break;
                }
            case PuzzleName.REPEAT_MELODY:
                {
                    gameObject.AddComponent<RepeatMelody>();
                    break;
                }
        }
    }

}
